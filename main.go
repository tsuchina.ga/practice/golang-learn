package main

import (
    "./libs"
    _ "github.com/lib/pq"
    "database/sql"
    "log"
)

func main() {
    libs.LogOutput("START")

    db, err := sql.Open("postgres", "dbname=golang host=golang-psql port=5432 user=golang password=golang sslmode=disable")

    if err != nil {
        log.Println(err)
        return
    }
    defer db.Close()

    rows, err := db.Query("SELECT id, name, age, is_deleted FROM users;")
    if err != nil {
        log.Println(err)
        return
    }
    defer rows.Close()

    for rows.Next() {
        var id int
        var name string
        var age int
        var isDeleted int
        err = rows.Scan(&id, &name, &age, &isDeleted)

        log.Println(id, name, age, isDeleted)
    }

    libs.LogOutput("END")
}
