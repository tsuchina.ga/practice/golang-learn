# Go言語の練習


## 前提

* Windows 10 64bit Home
* IntelliJ IDEA


## 開発環境構築

0. インストールとダウンロード

0. GOROOTが環境変数に入っていることを確認

    ```
    > echo %GOROOT%
    C:\Go\
    ```
    
0. GOPATHの追加。場所は任意

0. `%GOPATH%/bin`をPATHに追加

0. depのセットアップ

    ```
    > go get -u github.com/golang/dep/cmd/dep
    > dep version
    dep:
     version     : devel
     build date  :
     git hash    :
     go version  : go1.10.1
     go compiler : gc
     platform    : windows/amd64
     features    : ImportDuringSolve=false
    ```

とりあえずこんな感じで。

今後なにかパッケージを追加したい場合は`dep ensure -add {package name}`って感じで


## デイレクトリ構成

    project root
    |\_libs
    |    なんか全体的に使いそうな関数
    |\_log
    |    ログ出力先
    |\_vendor
    |\_Gopkg.lock
    |\_Gopkg.toml
    |    vendor, Gopkgは依存関係を解決するためのん
    |\_main.go
    |    実行ファイル
     \_README.md
         このファイル

libsには各APIを叩くための諸々の処理をいれることになると思う。
     
libs以外にもmodelsやservicesなんかを追加することがあるかも。


## Avastがクイックスキャンをやめない

設定からスキャン除外を設定でき、下記を除外するようにします。

`%USERPROFILE%\AppData\Local\Temp\___*go_build_main_go.exe`

あ、`%USERPROFILE%`は解決してから登録してね。そのままだとダメっぽいよ


## dockerで実行する

GOPATH汚染もたいがいですが、ローカルのデータベースを汚染されるのも嫌です。
なのでデータベースアクセスなどが必要な場合はDockerに追い出してしまいましょう。

参照: `docker-compose.yml`

前提としてDocker、docker-composeはインストールされているものとします。

0. `docker-compose up -d`で起動

0. `docker exec -it golang bash`でgolangにアクセス

0. `go run main.go`を実行出来たらOK

SQLサーバは`sql`ディレクトリのクエリを名前順に実行するので、テストデータはそこでいれておこう。
