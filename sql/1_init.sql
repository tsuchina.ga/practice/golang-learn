Drop Table If Exists users;
CREATE TABLE users (
  id serial primary key Not Null,
  name varchar(32) Not Null,
  age int Not Null Default 0,
  is_deleted smallint Not Null Default 0,
  created_at timestamp Not Null Default CURRENT_TIMESTAMP,
  updated_at timestamp Not Null Default CURRENT_TIMESTAMP
);

COMMENT ON COLUMN users.id IS 'ユーザーID(シリアル値・自動カウント)';
COMMENT ON COLUMN users.name IS 'ユーザー名';
COMMENT ON COLUMN users.age IS '年齢';
COMMENT ON COLUMN users.is_deleted IS '削除状況(1: 削除済み)';
COMMENT ON COLUMN users.created_at IS '登録日時';
COMMENT ON COLUMN users.updated_at IS '更新日時';
COMMENT ON TABLE users IS 'ユーザー' ;

