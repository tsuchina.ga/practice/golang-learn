package libs

import (
    "log"
    "os"
    "time"
)

func LogOutput(s string) {
    log.Println(s)

    date := time.Now().Format("20060102")

    file, err := os.OpenFile("./log/" + date + ".log", os.O_APPEND | os.O_WRONLY, 0600)

    if err != nil {
        file, err = os.Create("./log/" + date + ".log")
    }
    defer file.Close()

    if err != nil {
        log.Println(err)
    } else {
        file.WriteString(time.Now().Format("2006-01-02T15:04:05.0000Z07:00") + " " + s + "\n")
    }
}
